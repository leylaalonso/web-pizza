<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get("/product", 'ProductController@getAll');
Route::post("/order", 'OrderController@createOrder');
Route::get("/exchange", 'ExchangeController@usdExchangeRate');
Route::get("/shipping", 'ShippingController@getShippingRate');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

