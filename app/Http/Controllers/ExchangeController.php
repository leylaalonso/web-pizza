<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;

use Illuminate\Http\Request;

class ExchangeController extends Controller
{
    function usdExchangeRate(){
        $response = Http::get('https://api.exchangeratesapi.io/latest?base=EUR');
        $exchange = $response->json()['rates']['USD'];
        return [ 'usdExchange' => $exchange ];
    }
}
