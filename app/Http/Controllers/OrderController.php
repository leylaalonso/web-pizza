<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\OrderDetails;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ExchangeController;

use Illuminate\Http\Request;

class OrderController extends Controller
{

    protected $exhangeController;

    public function __construct(ExchangeController $exhangeController)
    {
        $this->exhangeController = $exhangeController;
    }

    function createOrder(Request $request){
        $validator = Validator::make($request->all(),[
            'customer_name' =>'required|max:50',
            'delivery_address' =>'required',
            'phone' =>'required',
            'zipcode'=>'required',
            'email'=>'required|email',
            'currency'  =>'required',
            'items'  =>'required|min:1',
            'items.*.product_id'  =>'required|exists:App\Product,id',
            'items.*.amount'  =>'required|min:1',
        ]);
        if ($validator->fails()){
            $errors = $validator->errors();
            return $errors->toJson();
        }
        $order = new Order;
        $order->customer_name = $request->input("customer_name");
        $order->delivery_address = $request->input("delivery_address");
        $order->address_detail = $request->input("address_detail");
        $order->zipcode = $request->input("zipcode");
        $order->phone = $request->input("phone");
        $order->email = $request->input("email");
        $order->currency = $request->input("currency");
        $order->exchange_rate = $this->exhangeController->usdExchangeRate()['usdExchange'];
        $order->total_price = 0;
        $order->save();
        foreach ($request->input("items") as $item) {
            $orderDetail = new OrderDetails();
            $orderDetail->order_id = $order->id;
            $orderDetail->product_id = $item['product_id'];
            $orderDetail->amount = $item['amount'];
            $product = Product::find($item['product_id']);
            $orderDetail->unit_price_eu = $product->price_eu;   
            $order->total_price += $product->price_eu * $item['amount']; 
            $orderDetail->save();
        }
        $order->save();
        return ['orderId' => $order->id];
    }
}
