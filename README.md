# Onclick Pizza

## Technologies used:

  **Backend:** Laravel, PostgreSQL

  **Frontend:** React (with hooks), Typescript, Bootstrap (Reactstrap), Redux, React router dom

All Communication between backend and frontend are made with REST.

The exchange rate is consulted from a public api ( https://exchangeratesapi.io/ ) 

All the products and orders are saved in the database, when an order is placed successfully the application gives you a unique order number.

The application saves the information on the browser, so if you close the webpage by mistake and then you open it again the information is not los 

This project’s frontend was bootstrapped with [Create React App Laravel](https://github.com/mjsarfatti/create-react-app-laravel).

## Available Scripts

In the project directory, you can run:

### `npm run start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to Laravel’s `public` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


