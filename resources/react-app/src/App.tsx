import React from 'react';
import { useDispatch } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.scss';
import useMount from './app/hook/useMount';
import HomeScreen from './app/screens/HomeScreen';
import SuccessScreen from './app/screens/SuccessScreen';
import EphectaPizzaApi from './app/services/EphectaPizzaApi';
import { loadUsdExchange } from './app/store/config';
import { loadProducts } from './app/store/products';
import Order from './app/screens/Order';

function App() {
  const dispatch = useDispatch();

  useMount(async () => {    
    try {
      const exchange = await EphectaPizzaApi.getUSDExchange();
      dispatch(loadUsdExchange(exchange));
    }catch(e){
      console.error(e)
    }
  });
  
  useMount(async () => {    
    try {
      const products = await EphectaPizzaApi.getMenu();
      dispatch(loadProducts(products));
    }catch(e){
      console.error(e)
    }
  });
  

  return (
    <Router>
      <Switch>
        <Route exact path={["/", "/pizza/:id"]} component={HomeScreen}/>
        <Route exact path={["/order"]} component={Order}/>
        <Route exact path="/success/:orderId" component={SuccessScreen}/>
        <Route>
          <h1>404</h1>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
