import { faDollarSign, faEuroSign, faShoppingCart, faArrowUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { ReactNode, useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Badge, Button, ButtonGroup } from 'reactstrap';
import logo from '../../assets/logo.svg';
import MoneyType from '../../model/MoneyType';
import { setCurrency } from '../../store/order';
import { StoreState } from '../../store/store';
import { useHistory, Link } from 'react-router-dom';

interface Props {
    children : ReactNode
}

const Template = (props : Props) => {
  const currency = useSelector((state:StoreState) => state.order.currency)
  const items = useSelector((state:StoreState) => state.order.items)
  const totalAmount =  items.reduce((acc,i)=>i.amount + acc,0);
  const dispatch = useDispatch();
  const history = useHistory();

  const [scrollUp, setScrollUp] = useState<number>(0)
  const showScrollUp = scrollUp > 20;

  const scrollToTop = ()=>{
    window.scrollTo({
     top:0,
     behavior:"smooth", 
    })
  }

  const onScroll = useCallback(
    () => {
      setScrollUp(window.scrollY)
    },
    [],
  )

  useEffect(()=>{
    window.addEventListener("scroll",onScroll);
    return ()=>{
      window.removeEventListener("scroll",onScroll);
    }
  }
  ,[onScroll])


  return (
    <>
    <header>
      <div className="navbar navbar-light bg-white box-shadow fixed-top shadow ">
        <div className="container d-flex justify-content-between">
          <Link to="/">
            <span className="navbar-brand">
              <img src={logo}  height="32px" alt="OnClickPizza"/>
              <span className="d-none d-sm-inline brand">
                <span className="ephecta">OnClick</span>
                <span className="pizza">Pizza</span>
              </span>
            </span>
          </Link>
          <span>
            <ButtonGroup>
              <Button 
                color="primary" 
                onClick={()=>dispatch(setCurrency(MoneyType.euro))} 
                active={currency===MoneyType.euro}>
                <FontAwesomeIcon icon={faEuroSign} />
              </Button>
              <Button
                className="mr-5" 
                color="primary" 
                onClick={()=>dispatch(setCurrency(MoneyType.dollar))} 
                active={currency===MoneyType.dollar}>
                <FontAwesomeIcon icon={faDollarSign} />
              </Button>
            </ButtonGroup>
            <Button onClick={()=> history.push(`/order`) } className="position-relative">
              <Badge color="primary" className="corner-badge">
                { 
                  totalAmount
                }
              </Badge>
              <FontAwesomeIcon icon={faShoppingCart}/>
            </Button>
          </span>
        </div>
      </div>
    </header>
    <main>
      {props.children}
      <Button 
        onClick={scrollToTop}
        className="scroll-to-top" 
        hidden={!showScrollUp}>
        <FontAwesomeIcon icon={faArrowUp} />
      </Button>
    </main>
    <footer className="py-4 text-muted bg-dark fixed-bottom">
      <div className="d-flex justify-content-around">
        <span>©2020 - OnClick Pizza</span>
        <span>123 Fake Street - Springfield</span>
      </div>
    </footer>
    </>
  );
};

export default Template;
