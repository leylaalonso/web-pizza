export interface Product {
  id: number,
  code: string,
  name: string,
  price_eu: number,
  description: string,
}