import MoneyType from "./MoneyType";

export interface OrderDetail {
  product_id: number;
  amount: number;
}

export interface Order {
  customer_name: string;
  delivery_address: string;
  address_detail: string;
  zipcode:string;
  phone: string;
  currency: MoneyType;
  email:string;
  items: OrderDetail[];
}

