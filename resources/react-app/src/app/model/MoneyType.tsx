enum MoneyType {
  euro = "EUR",
  dollar = "USD"
}

export default MoneyType;