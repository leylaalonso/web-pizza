import React from 'react'
import { OrderDetail } from '../model/Order'
import { useSelector, useDispatch } from 'react-redux';
import { StoreState } from '../store/store';
import { ListGroupItem, Row, Col, Button, InputGroup, InputGroupAddon, Input } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import { removeProduct, incrementProduct, decrementProduct } from '../store/order';
import Price from './Price';

interface Props {
  orderDetail: OrderDetail
}

const OrderProductItem = ({orderDetail}: Props) => {
  const products = useSelector((state:StoreState) => state.products)
  const dispatch = useDispatch();
  
    const product = products.find((p) => p.id === orderDetail.product_id);
    if (!product) {
      return null;
    }
    return (
      <ListGroupItem className="justify-content-between">
        <Row>
          <Col md="1" sm="1" className="text-right">
            <Button outline onClick={()=> dispatch(removeProduct(product.id))}>
              <FontAwesomeIcon icon={faTrash} />
            </Button>
          </Col>
          <Col md="6">
            <div style={{fontSize: '1.5em'}}>
              <strong>{product.name}</strong>
            </div>
            <small color="muted">{product.description}</small>
          </Col>
          <Col md="3"className="text-right">
            <span style={{fontSize: '1.5em'}}>
              <Price value={product.price_eu * orderDetail.amount} />
            </span>
          </Col>
          <Col md="2">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button onClick={()=> dispatch(incrementProduct({amount:1,product_id:product.id}))}>
                  <FontAwesomeIcon icon={faPlus}/>
                </Button>
              </InputGroupAddon>
              <Input readOnly value={orderDetail.amount} />
              <InputGroupAddon addonType="append">
                <Button onClick={()=> dispatch(decrementProduct({amount:1,product_id:product.id}))}>
                  <FontAwesomeIcon icon={faMinus}/>
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </Row>
      </ListGroupItem>
    );
}

export default OrderProductItem
