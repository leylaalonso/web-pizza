import React from 'react'
import MoneyType from '../model/MoneyType'
import { useSelector } from 'react-redux'
import { StoreState } from '../store/store'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEuroSign, faDollarSign } from '@fortawesome/free-solid-svg-icons'

interface Props {
  value: number;
}

const Price = ({value}: Props) => {
  const currency = useSelector((state: StoreState) => state.order.currency)
  const usdExchangeRate = useSelector((state:StoreState) => state.config)

  if (!value) {
    return null;
  }

  if (currency === MoneyType.euro) {
    return <span><FontAwesomeIcon icon={faEuroSign} />{ Number(value).toFixed(2) }</span>
  }

  return <span><FontAwesomeIcon icon={faDollarSign} />{ (Number(value) * usdExchangeRate).toFixed(2) }</span>
}

export default Price
