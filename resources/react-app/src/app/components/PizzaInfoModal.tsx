import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { Card, CardBody, CardImg, CardText, CardTitle, Modal, ModalBody } from 'reactstrap';
import { StoreState } from '../store/store';
import Price from './Price';

const IMAGE_BASE = (process.env.REACT_APP_BASE_URL ?? '') + '/img/pizza/'

const PizzaInfoModal = () => {
  const history = useHistory();
  const { id } = useParams();
  const products = useSelector((state:StoreState) => state.products)
  const closeModal = () => {
    history.push("/");
  }

  const product = products?.find((p) => p.id.toString() === id );

  return (
    <Modal toggle={closeModal} isOpen={id}>
      <ModalBody>
        <Card>
          <CardImg top src={IMAGE_BASE + product?.code + ".jpg"}  />
          <CardBody>
            <CardTitle>
              {product?.name} 
              {' / '}
              <strong><Price value={product?.price_eu ?? 0} /></strong>
            </CardTitle>
            <CardText>{product?.description} </CardText>
          </CardBody>
        </Card>
      </ModalBody>
    </Modal>
  )
}

export default PizzaInfoModal
