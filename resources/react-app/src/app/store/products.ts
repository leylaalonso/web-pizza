import { createAction, createReducer } from '@reduxjs/toolkit';
import { Product } from '../model/Product';

export const loadProducts = createAction<Product[]>('loadProducts');

const initState: Product[] = [];

export default createReducer(initState, builder => 
  builder.addCase(loadProducts,(state,action) => action.payload ),
);