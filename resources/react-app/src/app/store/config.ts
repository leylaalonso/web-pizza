import { createAction, createReducer } from '@reduxjs/toolkit';

export const loadUsdExchange = createAction<number>('loadUsdExchange');

const initState: number = 1;

export default createReducer(initState, builder => 
  builder.addCase(loadUsdExchange,(state,action) => action.payload ),
);