import { configureStore, combineReducers } from '@reduxjs/toolkit'
import products from './products';
import order from './order';
import config from './config';
import { loadState, saveState } from './persistanceStorage';

const rootReducer = combineReducers({
  products,
  order,
  config,
})

const initialState = loadState();

export const store = configureStore({reducer:rootReducer, preloadedState: initialState });

store.subscribe(() => {
  saveState(store.getState());
});

export type StoreState = ReturnType<typeof rootReducer>