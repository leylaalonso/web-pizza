import { createAction, createReducer } from '@reduxjs/toolkit';
import MoneyType from '../model/MoneyType';
import { Order } from '../model/Order';

interface ChangeOrderAmount{
  product_id: number,
  amount: number,
}


export const setCurrency = createAction<MoneyType>('setCurrency');
export const setCustomerName = createAction<string>('setCustomerName');
export const setEmail = createAction<string>('setEmail');
export const setAddressDetail = createAction<string>('setAddressDetail');
export const setZipcode = createAction<string>('setZipcode');
export const setDeliveryAddress = createAction<string>('setDeliveryAddress');
export const setPhone = createAction<string>('setPhone');

export const incrementProduct = createAction<ChangeOrderAmount>('incrementProduct');
export const decrementProduct = createAction<ChangeOrderAmount>('decrementProduct');
export const removeProduct = createAction<number>('removeProduct');

export const clearOrder = createAction<void>('clearOrder');

const initState: Order = {
  currency: MoneyType.euro,
  customer_name: '',
  delivery_address: '',
  phone: '',
  address_detail:'',
  zipcode:'',
  email:'',
  items: [],
};

export default createReducer(initState, builder => 
  builder.addCase(setCurrency,(state,action) => ({...state,currency: action.payload}) )
    .addCase(setCustomerName,(state,action) => ({...state,customer_name: action.payload}) )
    .addCase(setEmail,(state,action) => ({...state,email: action.payload}) )
    .addCase(setDeliveryAddress,(state,action) => ({...state,delivery_address: action.payload}) )
    .addCase(setAddressDetail,(state,action) => ({...state,address_detail: action.payload}) )
    .addCase(setZipcode,(state,action) => ({...state,zipcode: action.payload}) )
    .addCase(setPhone,(state,action) => ({...state,phone: action.payload}))
    .addCase(incrementProduct,(state,action) => {
      const productExist = state.items.some((i) => i.product_id === action.payload.product_id);
      return  {
        ...state,
        items: !productExist ? state.items.concat(action.payload)
          : state.items.map((i) => i.product_id === action.payload.product_id 
            ? {product_id:i.product_id, amount: i.amount + action.payload.amount } 
            : i 
          ) 
      }
    }
    ).addCase(decrementProduct,(state,action) => {
      return  {
        ...state,
        items: state.items.map((i) => i.product_id === action.payload.product_id 
          ? {product_id:i.product_id, amount: Math.max(1,i.amount - action.payload.amount) } 
          : i 
        )
      }
    }).addCase(removeProduct, (state,action)=> (
      {...state, items: state.items.filter((i)=>i.product_id !== action.payload)}
    )).addCase(clearOrder, (state,action)=> initState)
);