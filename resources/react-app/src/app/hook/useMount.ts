import { useEffect } from "react"

const useMount = (onInit: ()=>unknown) =>{
  useEffect(() => {
    onInit();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
}

export default useMount;