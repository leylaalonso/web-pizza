import api from "../../config/api";
import { Order } from "../model/Order";
import { Product } from "../model/Product";


export default class EphectaPizzaApi {
  static async getMenu  () : Promise<Product[]>{
    const response = await api.get<Product[]>("/product");
    return response.data;
  }

  static async order(order: Order) : Promise<number>{
    const response = await api.post<{orderId:number}>("/order",order);
    return response.data.orderId;
  } 

  static async getUSDExchange() : Promise<number>{
    const response = await api.get<{usdExchange:number}>("/exchange");
    return response.data.usdExchange;
  }

  static async getShippingRate(zipcode: string) : Promise<number>{
    const response = await api.get<{rate:number}>("/shipping", {params:{ zipcode }} );
    return response.data.rate;
  }
}