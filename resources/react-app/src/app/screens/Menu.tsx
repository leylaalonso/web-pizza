import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Button, Card, CardBody, CardImg, CardText, CardTitle, Col, Container, Row, Spinner } from 'reactstrap';
import Price from '../components/Price';
import { incrementProduct } from '../store/order';
import { StoreState } from '../store/store';

const IMAGE_BASE = (process.env.REACT_APP_BASE_URL ?? '') + '/img/pizza/'

const Menu = () => {
  const dispatch = useDispatch();
  const products = useSelector((state:StoreState) => state.products)
  const history = useHistory();

  const openModal = (productId: number) => {
    history.push(`/pizza/${productId}`);
  }

  if(products.length < 1) {
    return <Spinner color="primary" />
  }

  return (
    <Container style={{marginBottom:"69px"}}>
      <h2>Menu</h2>
      <Row>
        {products.map((p)=>(
          <Col md={3} key={p.id}>
            <Card 
              className="mb-4 shadow selectable"
              onClick={() => openModal(p.id)}
            >
              <CardImg top src={IMAGE_BASE + p.code + ".jpg"}  />
              <CardBody>
                <CardTitle>
                  {p.name} 
                  {' / '}
                  <strong><Price value={p.price_eu} /></strong>
                </CardTitle>
                <CardText className="crop-text">{p.description} </CardText>
                <Button className="btn-block" onClick={(e)=>{
                  e.stopPropagation();
                  dispatch(incrementProduct({amount:1,product_id:p.id}));
                }}>
                  <FontAwesomeIcon icon={faPlus}/>
                  <span className="m-4"><strong>Add</strong></span>
                </Button>
              </CardBody>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  ) ;
};

export default Menu;
