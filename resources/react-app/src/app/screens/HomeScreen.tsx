import React from 'react';
import { Container } from 'reactstrap';
import Template from '../containers/Template';
import Menu from './Menu';
import style from './Menu.module.scss';
import PizzaInfoModal from '../components/PizzaInfoModal';

const HomeScreen = () => {

  return (
    <Template>
      <section className={`jumbotron ${style.bgImage}`}>
        <Container>
          
          <h1 className="brand brand-big d-none d-md-block">
            <span className="ephecta">OnClick</span>
            <span className="pizza">Pizza</span>
          </h1>
          <h1 className="brand d-md-none">
            <span className="ephecta">OnClick</span>
            <span className="pizza">Pizza</span>
          </h1>
        </Container>
      </section>
      <Menu />
      <PizzaInfoModal />
    </Template>
  ) ;
};

export default HomeScreen;
