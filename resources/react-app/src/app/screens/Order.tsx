import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Col, Container, FormFeedback, FormGroup, Input, Label, ListGroup, ListGroupItem, Row, Alert, Spinner } from 'reactstrap';
import OrderProductItem from '../components/OrderProductItem';
import Price from '../components/Price';
import EphectaPizzaApi from '../services/EphectaPizzaApi';
import { clearOrder, setAddressDetail, setCustomerName, setDeliveryAddress, setPhone, setZipcode, setEmail } from '../store/order';
import { StoreState } from '../store/store';
import { useHistory } from 'react-router-dom';
import Template from '../containers/Template';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

const initValidationForm = {
  customer_name:false,
  delivery_address:false,
  phone:false,
  zipcode:false,
  items:false,
  email:false,
}

const Order = () => {
  
  const products = useSelector((state:StoreState) => state.products)
  const order = useSelector((state:StoreState) => state.order)
  const dispatch = useDispatch();
  const [shippingRate, setShippingRate] = useState<number|undefined>(undefined)
  const [validation, setValidation] = useState(initValidationForm);
  const history = useHistory();
  
  const validateForm = (): boolean =>{
    setValidation(initValidationForm);
    let isOk = true;
    if(!order.customer_name) {
      isOk = false; 
      setValidation((v) => ({...v, customer_name : true }))
    }
    if(!order.phone) {
      isOk = false;
      setValidation((v) => ({...v, phone : true }))
    }
    if(!order.delivery_address) {
      isOk = false; 
      setValidation((v) => ({...v, delivery_address : true }))
    }
    if(!order.zipcode) {
      isOk = false; 
      setValidation((v) => ({...v, zipcode : true }))
    }
    // eslint-disable-next-line
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!re.test(String(order.email).toLowerCase())) {
      isOk = false; 
      setValidation((v) => ({...v, email : true }))
    }
    if(order.items.length === 0) {
      isOk = false; 
      setValidation((v) => ({...v, items : true }))
    }
    return isOk;
  }

  const calculateShipping = async () =>{
    if (!order.zipcode){
      setValidation({...validation, zipcode:true})
      return; 
    }
    try {
      setShippingRate(await EphectaPizzaApi.getShippingRate(order.zipcode));
    }catch(e){
      console.error(e)
    }
  }

  useEffect(() => {
    setValidation((v) => ({...v, items: false}))
  }, [order.items]);

  const calculateSubTotal = order.items.reduce((acc, i)=>{
    const p = products.find((p)=> p.id === i.product_id);
    if(!p){
      return acc;
    }
    return acc + ( p.price_eu * i.amount );
  },0);
  

  const send = async () => {
    if (!validateForm()){
      return;
    }
    const orderId = await EphectaPizzaApi.order(order);
    dispatch(clearOrder());
    history.push(`/success/${orderId}`)
  }
 
  if(products.length < 1) {
    return <Spinner color="primary" />
  }

  return (
    <Template>
        <Container className="pt-4" style={{marginBottom:"69px"}}>
          <h2>My Order</h2>
          <Row>
            <Col md="4">
              <FormGroup>
                <Label >Name</Label>
                <Input 
                  type="text"
                  invalid={validation.customer_name}
                  value={order.customer_name} 
                  onChange={(e) => { 
                    dispatch(setCustomerName(e.target.value))
                    setValidation({...validation,customer_name:false})
                  }}/>
                <FormFeedback>Required input</FormFeedback>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label >Email</Label>
                <Input 
                  type="email"
                  invalid={validation.email}
                  value={order.email} 
                  onChange={(e) => { 
                    dispatch(setEmail(e.target.value))
                    setValidation({...validation,email:false})
                  }}/>
                <FormFeedback>Invalid email</FormFeedback>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label >Phone</Label>
                <Input
                  type="number" 
                  invalid={validation.phone}
                  value={order.phone}
                  onChange={(e) => {
                    dispatch(setPhone(e.target.value))
                    setValidation({...validation,phone:false})
                  }}/>
                <FormFeedback>Required input</FormFeedback>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md="6">
              <FormGroup>
                <Label>Delivery address</Label>
                <Input
                  invalid={validation.delivery_address} 
                  value={order.delivery_address}
                  onChange={(e) => {
                    dispatch(setDeliveryAddress(e.target.value))
                    setValidation({...validation,delivery_address:false})
                  }}/>
                <FormFeedback>Required input</FormFeedback>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>Apt. / Suite  </Label>
                <Input 
                  type="text" 
                  value={order.address_detail} 
                  onChange={(e) => dispatch(setAddressDetail(e.target.value))}
                />
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>Zipcode</Label>
                <Input 
                  type="number"
                  invalid={validation.zipcode} 
                  value={order.zipcode} onChange={(e) =>{
                    dispatch(setZipcode(e.target.value))
                    setShippingRate(undefined);
                    setValidation({...validation,zipcode:false})
                  }}/>
                <FormFeedback>Required input</FormFeedback>
              </FormGroup>
            </Col>
          </Row>
          <ListGroup className="text-dark">
            { 
              order.items.map((i)=> <OrderProductItem orderDetail={i} key={i.product_id} />)
            }
            {
              validation.items && <Alert color="danger">You must order at least one pizza</Alert>
            }
            <ListGroupItem className="text-md">
              <Row>
                <Col className="text-right" md="6" sm="6">
                  <span >Subtotal</span>
                </Col>
                <Col md="6" sm="6" className="text-right">
                  <Price value={calculateSubTotal} />
                </Col>
              </Row>
            </ListGroupItem>
            <ListGroupItem className="text-md">
              <Row>
                <Col className="text-right" md="6" sm="6">
                  <span >Shipping</span>
                </Col>
                <Col md="6" sm="6" className="text-right">
                  <Price value={shippingRate ?? 0} />
                </Col>
              </Row>
            </ListGroupItem>
            <ListGroupItem active className="text-lg text-dark" >
              <Row>
                <Col className="text-right" md="6" sm="6">
                  <strong >Total</strong>
                </Col>
                <Col md="6" sm="6" className="text-right">
                  <strong>
                    <Price value={shippingRate ? shippingRate + calculateSubTotal : calculateSubTotal} />
                  </strong>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
          <div className="d-flex justify-content-around flex-wrap mb-2">
            <Button className="my-4" size="md" color="primary" onClick={()=>history.push(`/`)}>
              <FontAwesomeIcon icon={faArrowLeft}/>
            </Button>
            <div className="my-4" hidden={shippingRate !== undefined}>
              <Button size="md" color="primary" onClick={()=>calculateShipping()}>
                Calculate Shipping
              </Button>
            </div>
            <div className="my-4" hidden={shippingRate === undefined}>
              <Button size="md" color="primary" onClick={send}>
                GET MY PIZZA !
              </Button>
            </div>
          </div>
        </Container>
    </Template>
  ) ;
};

export default Order;
