import React from 'react'
import Template from '../containers/Template'
import { useParams, Link } from 'react-router-dom'
import { Container } from 'reactstrap';

const SuccessScreen = () => {

  const {orderId} = useParams();

  return (
    <Template>
      <Container className="text-center">
        <h1 className="text-success pt-5">Your order was successful</h1>
        <h3>Your order code is <strong>{orderId}</strong> </h3>
        <Link to="/" className="btn btn-lg btn-secondary my-3" >GO BACK</Link>
      </Container>
    </Template>
  )
}

export default SuccessScreen
