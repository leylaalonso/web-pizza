<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        DB::table('products')->insert([
            'code' => 'MUZ',
            'name' => 'Muzarella',
            'price_eu' => 10.50,
            'description' => 'Tomatoe sauce and muzarella cheese',
        ]);
        DB::table('products')->insert([
            'code' => 'ANC',
            'name' => 'Anchovies',
            'price_eu' => 12.50,
            'description' => 'Tomatoe suace, muzarella cheese and anchovies',
        ]);
        DB::table('products')->insert([
            'code' => 'MJM',
            'name' => 'Jam and red peppers',
            'price_eu' => 14.99,
            'description' => 'Tomatoe sauce, muzarella cheese, jam and baked red peppers ',
        ]);
        DB::table('products')->insert([
            'code' => 'FUG',
            'name' => 'Fugazzeta',
            'price_eu' => 12.99,
            'description' => 'Muzarella cheese with sliced onions',
        ]);
        DB::table('products')->insert([
            'code' => 'NPL',
            'name' => 'Napolitana',
            'price_eu' => 14.50,
            'description' => 'Tomatoe sauce, muzarella cheese, sliced tomatoes and garlic',
        ]);
        DB::table('products')->insert([
            'code' => 'MGT',
            'name' => 'Margarita',
            'price_eu' => 12.50,
            'description' => 'Tomatoe sauce, muzarella cheese and basil',
        ]);
        DB::table('products')->insert([
            'code' => 'CLB',
            'name' => 'Calabreza',
            'price_eu' => 8.99,
            'description' => 'Tomatoe sauce, muzzarella and calabreza sausage',
        ]);
        DB::table('products')->insert([
            'code' => 'ARU',
            'name' => 'Arugula leafs',
            'price_eu' => 9.99,
            'description' => 'Tomatoe sauce, muzarella cheese, arugula leafs and grated cheeses',
        ]);
    }
}
